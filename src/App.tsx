import Grid from '@material-ui/core/Grid';
import { withSnackbar } from 'notistack';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import BottomBar from './component/BottomBar';
import { ConflictDialog } from './component/ConflictDialog';
import Database, { DatabaseClient } from './component/Database';
import GoogleSync, { GoogleSyncClient } from './component/GoogleSync';
import { SplashScreen } from './component/SplashScreen';
import SyncIndicator from './component/SyncIndicator';
import About from './scene/About';
import Forecasts from './scene/Forecasts';
import Home from './scene/Home';
import Lines from './scene/Lines';
import NewForecast from './scene/NewForecast';
import NewLine from './scene/NewLine';
import Sync from './scene/Sync';
import * as serviceWorker from './serviceWorker';
import { theme } from './util/theme';
import DateProvider from './component/DateProvider';

class App extends React.Component {
  componentDidMount() {
    const snackOption = {
      anchorOrigin: {
        horizontal: 'left',
        vertical: 'top',
      },
    };

    serviceWorker.register({
      onSuccess: () => {
        // @ts-ignore
        this.props.enqueueSnackbar("L'application peut désormais être utilisé hors ligne.", snackOption);
      },
      onUpdate: () => {
        // @ts-ignore
        this.props.enqueueSnackbar("Actualisez la page pour mettre à jour l'application.", snackOption);
      },
    });
  }

  render() {
    const mainSceneStyle: React.CSSProperties = {
      height: 'calc(100vh - 56px - 1em)',
      width: 'calc(100vw)',
      overflow: 'hide',
      overflowY: 'auto',
      paddingBottom: '1em',
      backgroundColor: theme.palette.background.default,
    };

    return (
      <GoogleSync>
        <GoogleSyncClient>
          {(gs) => (
            <Database
              onUpdate={gs.requestExport}
              importedData={gs.importedData}
              syncState={gs.status}
              onImport={gs.onceImported}
            >
              <DatabaseClient>
                {(db) =>
                  gs.conflict ? (
                    <ConflictDialog />
                  ) : gs.status === 'init' || gs.status === 'import' || !db.ready ? (
                    <SplashScreen />
                  ) : (
                    <DateProvider>
                      <Router>
                        <SyncIndicator status={gs.status} />
                        <Grid container direction="column" justify="space-between" alignItems="stretch">
                          <div style={mainSceneStyle}>
                            <Switch>
                              <Route exact path="/">
                                <Home />
                              </Route>
                              <Route exact path="/lines">
                                <Lines />
                              </Route>
                              <Route exact path="/lines/new">
                                <NewLine />
                              </Route>
                              <Route path="/lines/edit">
                                <DatabaseClient>
                                  {(db) => (
                                    <NewLine
                                      label="Modifier ligne"
                                      uri={window.location.pathname.split('/')[3]}
                                      edit={db.lines[window.location.pathname.split('/')[3]]}
                                    />
                                  )}
                                </DatabaseClient>
                              </Route>
                              <Route exact path="/forecasts">
                                <Forecasts />
                              </Route>
                              <Route exact path="/forecasts/new">
                                <NewForecast />
                              </Route>
                              <Route exact path="/settings">
                                <Sync />
                                <About />
                              </Route>
                            </Switch>
                          </div>
                          <BottomBar />
                        </Grid>
                      </Router>
                    </DateProvider>
                  )
                }
              </DatabaseClient>
            </Database>
          )}
        </GoogleSyncClient>
      </GoogleSync>
    );
  }
}

// @ts-ignore
export default withSnackbar(App);
