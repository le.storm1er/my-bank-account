import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { uuid } from 'uuidv4';
import { DatabaseClient, Expectation } from '../component/Database';
import MonthSelector from '../component/MonthSelector';
import YearSelector from '../component/YearSelector';
import { DateConsumer } from '../component/DateProvider';
import { Switch } from '@material-ui/core';

type NewForecastState = Expectation & {
  toForecasts: boolean;
  isNegative: boolean;
};

class NewForecast extends React.Component {
  state: NewForecastState = {
    toForecasts: false,
    isNegative: true,
    amount: 0,
    endMonth: null,
    endYear: null,
    label: '',
    lines: [],
    startMonth: 0,
    startYear: 0,
  };

  handleChange(field: string) {
    return (
      e: React.ChangeEvent<
        HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement | { name?: string | undefined; value: unknown }
      >,
    ) => {
      if (field === 'amount') {
        const val = (e?.target?.value as string) || '';
        const numVal = parseFloat(val.replace(',', '.'));
        this.setState((prevState) => ({
          ...prevState,
          [field]: numVal || 0,
        }));
        return;
      }

      const val = e?.target?.value;
      this.setState((prevState) => ({
        ...prevState,
        [field]: val,
      }));
    };
  }

  render() {
    const spacingStyle = {
      marginTop: '1em',
    };
    if (this.state.toForecasts) {
      return <Redirect to="/forecasts" />;
    }
    return (
      <Container>
        <Grid container direction="column" justify="flex-start" alignContent="center" alignItems="stretch">
          <Typography variant="h1">Nouvelle prévision</Typography>
          <Grid container direction="row" justify="center" alignItems="center" alignContent="center">
            <Typography variant="body1">Revenu</Typography>
            <Switch
              checked={this.state.isNegative}
              onChange={(e) => {
                const isNegative = !!e?.target?.checked;
                this.setState((prevState) => ({
                  ...prevState,
                  isNegative,
                }));
              }}
              name="isNegative"
              color="primary"
            />
            <Typography variant="body1">Dépense</Typography>
          </Grid>
          <TextField
            onChange={this.handleChange('label')}
            style={spacingStyle}
            id="label"
            label="Nom"
            value={this.state.label}
          />
          <TextField
            onChange={this.handleChange('amount')}
            style={spacingStyle}
            id="amount"
            label="Montant"
            type="number"
            value={Math.abs(this.state.amount) || ''}
          />
          <DateConsumer>
            {(date) => (
              <React.Fragment>
                <YearSelector
                  style={spacingStyle}
                  label="Début: Année"
                  year={this.state.startYear || date.year}
                  onChange={this.handleChange('startYear')}
                />
                <MonthSelector
                  style={spacingStyle}
                  label="Début: Mois"
                  month={this.state.startMonth || date.month}
                  onChange={this.handleChange('startMonth')}
                />
              </React.Fragment>
            )}
          </DateConsumer>
          <YearSelector
            canBeUndefined
            style={spacingStyle}
            label="Fin: Année"
            year={this.state.endYear || ''}
            onChange={this.handleChange('endYear')}
          />
          <MonthSelector
            canBeUndefined
            style={spacingStyle}
            label="Fin: Mois"
            month={this.state.endMonth || ''}
            onChange={this.handleChange('endMonth')}
          />
          <DateConsumer>
            {(date) => (
              <DatabaseClient>
                {(db) => (
                  <Button
                    variant="contained"
                    color="primary"
                    style={spacingStyle}
                    onClick={this.createExpectation(date, db.writeExpectation)}
                  >
                    Créer
                  </Button>
                )}
              </DatabaseClient>
            )}
          </DateConsumer>
        </Grid>
      </Container>
    );
  }

  createExpectation(
    date: { month: number; year: number },
    writeExpectation: (uri: string, expectation: Expectation) => void,
  ): ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined {
    return () => {
      let newExpectation: Expectation = this.state;
      if (!newExpectation.endMonth || !newExpectation.endYear) {
        newExpectation.endMonth = null;
        newExpectation.endYear = null;
      }
      newExpectation.startYear = newExpectation.startYear ? newExpectation.startYear : date.year;
      newExpectation.startMonth = newExpectation.startMonth ? newExpectation.startMonth : date.month;
      if (this.state.isNegative) {
        newExpectation.amount *= -1;
      }
      const newUri = uuid();
      // @ts-ignore
      delete newExpectation.toForecasts;
      // @ts-ignore
      delete newExpectation.isNegative;
      writeExpectation(newUri, newExpectation);
      this.setState((prevState) => ({
        ...prevState,
        toForecasts: true,
      }));
    };
  }
}

export default NewForecast;
