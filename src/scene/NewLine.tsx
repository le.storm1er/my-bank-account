import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { uuid } from 'uuidv4';
import { DatabaseClient, Expectation, Line } from '../component/Database';
import { DateConsumer } from '../component/DateProvider';
import MonthSelector from '../component/MonthSelector';
import YearSelector from '../component/YearSelector';
import { ExpectationKeysInGivenMonth } from '../util/ExpectationInGivenMonth';

type NewLineState = Line & {
  toLines: boolean;
  isNegative: boolean;
};

type NewLineProps = {
  label?: string;
  uri?: string;
  edit?: Line;
};

class NewLine extends React.Component<NewLineProps, NewLineState> {
  constructor(props: NewLineProps) {
    super(props);
    let defaultLine: Line = {
      amount: 0,
      label: '',
      expectation: null,
      month: 0,
      year: 0,
    };
    if (this.props.uri && this.props.edit) {
      defaultLine = this.props.edit;
    }

    this.state = {
      toLines: false,
      isNegative: defaultLine.amount <= 0,
      ...defaultLine,
      amount: Math.abs(defaultLine.amount),
    };
  }

  handleChange(field: string) {
    return (
      e: React.ChangeEvent<
        HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement | { name?: string | undefined; value: unknown }
      >,
    ) => {
      if (field === 'amount') {
        const val = (e?.target?.value as string) || '';
        const numVal = parseFloat(val.replace(',', '.'));
        this.setState((prevState) => ({
          ...prevState,
          [field]: numVal || 0,
        }));
        return;
      }

      const val = e?.target?.value;
      this.setState((prevState) => ({
        ...prevState,
        [field]: val,
      }));
    };
  }

  handleAutocomplete():
    | ((
        event: React.ChangeEvent<{}>,
        value: (Expectation & { uri: string }) | null,
        reason: 'create-option' | 'select-option' | 'remove-option' | 'blur' | 'clear',
      ) => void)
    | undefined {
    return (event, value, reason) => {
      if (value !== null) {
        this.setState((prevState) => ({
          ...prevState,
          expectation: value.uri,
        }));
      } else {
        this.setState((prevState) => ({
          ...prevState,
          expectation: null,
        }));
      }
    };
  }

  render() {
    const spacingStyle = {
      marginTop: '1em',
    };
    if (this.state.toLines) {
      return <Redirect to="/lines" />;
    }
    return (
      <Container>
        <Grid container direction="column" justify="flex-start" alignContent="center" alignItems="stretch">
          <Typography variant="h1">{this.props.label ? this.props.label : 'Nouvelle ligne'}</Typography>
          <Grid container direction="row" justify="center" alignItems="center" alignContent="center">
            <Typography variant="body1">Revenu</Typography>
            <Switch
              checked={this.state.isNegative}
              onChange={(e) => {
                const isNegative = !!e?.target?.checked;
                this.setState((prevState) => ({
                  ...prevState,
                  isNegative,
                }));
              }}
              name="isNegative"
              color="primary"
            />
            <Typography variant="body1">Dépense</Typography>
          </Grid>
          <TextField
            onChange={this.handleChange('label')}
            style={spacingStyle}
            id="label"
            label="Nom"
            value={this.state.label}
          />
          <TextField
            onChange={this.handleChange('amount')}
            style={spacingStyle}
            id="amount"
            label="Montant"
            type="number"
            value={Math.abs(this.state.amount) || ''}
          />
          <DateConsumer>
            {(date) => (
              <React.Fragment>
                <YearSelector
                  style={spacingStyle}
                  label="Année"
                  year={this.state.year || date.year}
                  onChange={this.handleChange('year')}
                />
                <MonthSelector
                  style={spacingStyle}
                  label="Mois"
                  month={this.state.month || date.month}
                  onChange={this.handleChange('month')}
                />
                <DatabaseClient>
                  {(db) => {
                    const filter = {
                      month: this.state.month || date.month,
                      year: this.state.year || date.year,
                    };
                    const availableExpectations = Object.keys(db.expectations)
                      .filter(ExpectationKeysInGivenMonth(filter, db))
                      .map((expUri) => ({ ...db.expectations[expUri], uri: expUri }));

                    const selectedExpectation = this.state.expectation
                      ? { ...db.expectations[this.state.expectation], uri: this.state.expectation }
                      : undefined;

                    return (
                      <React.Fragment>
                        <Autocomplete
                          style={spacingStyle}
                          multiple={false}
                          id="expectation-autocomplete"
                          options={availableExpectations}
                          getOptionLabel={(exp) => exp.label}
                          renderOption={(option) => <span>{option.label}</span>}
                          renderInput={(params) => <TextField {...params} label="Prévision correspondante" />}
                          getOptionSelected={(exp, val) => {
                            return exp && val && exp.uri === val.uri;
                          }}
                          onChange={this.handleAutocomplete()}
                          value={selectedExpectation}
                        />
                        <DateConsumer>
                          {(date) => (
                            <Button
                              variant="contained"
                              color="primary"
                              style={spacingStyle}
                              onClick={this.createLine(date, db.writeLine)}
                            >
                              {this.props.uri && this.props.edit ? 'Modifier' : 'Créer'}
                            </Button>
                          )}
                        </DateConsumer>
                      </React.Fragment>
                    );
                  }}
                </DatabaseClient>
              </React.Fragment>
            )}
          </DateConsumer>
        </Grid>
      </Container>
    );
  }

  createLine(
    date: { month: number; year: number },
    writeLine: (uri: string, line: Line) => void,
  ): ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void) | undefined {
    return () => {
      let newLine: Line = this.state;
      const uri = this.props.uri ? this.props.uri : uuid();

      if (this.state.isNegative) {
        newLine.amount *= -1;
      }
      // @ts-ignore
      delete newLine.toLines;
      // @ts-ignore
      delete newLine.isNegative;
      newLine.year = newLine.year ? newLine.year : date.year;
      newLine.month = newLine.month ? newLine.month : date.month;
      writeLine(uri, newLine);
      this.setState((prevState) => ({
        ...prevState,
        toLines: true,
      }));
    };
  }
}

export default NewLine;
