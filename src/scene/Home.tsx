import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { DatabaseClient, DatabaseState } from '../component/Database';
import DateSelector from '../component/DateSelector';
import ExpectationsTable from '../component/ExpectationsTable';
import MonthStateDisplay from '../component/MonthStateDisplay';
import getPreviousMonthState from '../util/getPreviousMonthState';
import MoneyFormatter from '../util/MoneyFormatter';
import { DateConsumer } from '../component/DateProvider';

type HomeState = {
  startMonthModal: boolean;
  editStartMonthAmount: number | null;
};

class Home extends React.Component<{}, HomeState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      startMonthModal: false,
      editStartMonthAmount: null,
    };
  }

  renderStartMonthModal = (db: DatabaseState, month: number, year: number) => {
    const currentMonthStateUri = `${year}/${month}`;
    const previousMonthState = getPreviousMonthState(db.monthStates, currentMonthStateUri);
    let currentMonthState = db.monthStates[currentMonthStateUri];
    if (!currentMonthState) {
      currentMonthState = {
        expectedMovement: 0,
        month: month,
        realMovement: 0,
        realStartAmount: 0,
        year: year,
      };
    }
    const expectedCurrentMonthStartAmount = previousMonthState.realStartAmount + previousMonthState.realMovement;
    const expectedCurrentMonthStartAmountString = MoneyFormatter.format(expectedCurrentMonthStartAmount);

    return this.state.startMonthModal ? (
      <Dialog
        open
        onClose={() => this.setState((prevState) => ({ ...prevState, startMonthModal: false }))}
        aria-labelledby="dialog-real-start-month-title"
        aria-describedby="dialog-real-start-month-desc"
      >
        <DialogTitle id="dialog-real-start-month-title">Mettre à jour le début de mois</DialogTitle>
        <DialogContent>
          <DialogContentText id="dialog-real-start-month-desc">
            Le montant attendu calculé avec les données du mois précédent est {expectedCurrentMonthStartAmountString}.
          </DialogContentText>
        </DialogContent>
        <DialogContent>
          <Grid container direction="column">
            <TextField
              type="number"
              value={this.state.editStartMonthAmount}
              onChange={(e) => {
                const editStartMonthAmount = parseFloat(e?.target?.value) || null;
                this.setState((prevState) => ({
                  ...prevState,
                  editStartMonthAmount,
                }));
              }}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => this.setState((prevState) => ({ ...prevState, startMonthModal: false }))}
            color="primary"
          >
            Annuler
          </Button>
          <DatabaseClient>
            {(db) => {
              const onClick = () => {
                db.writeMonthState({
                  ...currentMonthState,
                  realStartAmount: this.state.editStartMonthAmount || 0,
                });
                this.setState((prevState) => ({
                  ...prevState,
                  startMonthModal: false,
                  editStartMonthAmount: null,
                }));
              };
              return (
                <Button onClick={onClick} color="primary">
                  Valider
                </Button>
              );
            }}
          </DatabaseClient>
        </DialogActions>
      </Dialog>
    ) : null;
  };

  render() {
    const spacingStyle = { marginTop: '1em' };
    return (
      <Container>
        <Typography variant="h1">Résumé</Typography>
        <DateConsumer>
          {(date) => (
            <React.Fragment>
              <DateSelector
                month={date.month}
                year={date.year}
                onMonthChange={date.setMonth}
                onYearChange={date.setYear}
              />
              <Button
                style={spacingStyle}
                onClick={() => {
                  this.setState((prevState) => ({ ...prevState, startMonthModal: true }));
                }}
              >
                Mettre à jour début de mois réel
              </Button>
              <MonthStateDisplay month={date.month} year={date.year} />
              <Typography style={spacingStyle} variant="h2">
                Prévisions
              </Typography>
              <ExpectationsTable style={spacingStyle} month={date.month} year={date.year} />
              <DatabaseClient>{(db) => this.renderStartMonthModal(db, date.month, date.year)}</DatabaseClient>
            </React.Fragment>
          )}
        </DateConsumer>
      </Container>
    );
  }
}

export default Home;
