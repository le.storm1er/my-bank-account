import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { DatabaseClient } from '../component/Database';
import ExpectationsList from '../component/ExpectationsList';
import MonthSelector from '../component/MonthSelector';
import YearSelector from '../component/YearSelector';
import DateSelector from '../component/DateSelector';
import { DateConsumer } from '../component/DateProvider';

type ForecastsState = {
  stopModal: string | null;
  removeModal: string | null;
  renameModal: string | null;
  stopMonth: number | null;
  stopYear: number | null;
  name: string | null;
};

class Forecasts extends React.Component<{}, ForecastsState> {
  state = {
    stopModal: null,
    removeModal: null,
    renameModal: null,
    stopMonth: new Date().getMonth(),
    stopYear: new Date().getFullYear(),
    name: null,
  };

  setStopModalExpId = (uri: string | null) => {
    this.setState((prevState) => ({
      ...prevState,
      stopModal: uri,
    }));
  };

  setRemoveModalExpId = (uri: string | null) => {
    this.setState((prevState) => ({
      ...prevState,
      removeModal: uri,
    }));
  };

  setRenameModalExpId = (uri: string | null) => {
    this.setState((prevState) => ({
      ...prevState,
      renameModal: uri,
    }));
  };

  renderStopModal = () => {
    return this.state.stopModal !== null ? (
      <Dialog open onClose={() => this.setStopModalExpId(null)} aria-labelledby="dialog-stop-charge-title">
        <DialogTitle id="dialog-stop-charge-title">Arrêter la répétition</DialogTitle>
        <DialogContent>
          <DialogContentText>Choisissez le dernier mois où cette ligne s'applique.</DialogContentText>
          <Grid container direction="column">
            <YearSelector
              canBeUndefined
              label="Année"
              year={this.state.stopYear || ''}
              onChange={(e) => {
                this.setState((prevState) => ({
                  ...prevState,
                  stopYear: (e?.target?.value as number) || null,
                }));
              }}
            />
            <MonthSelector
              style={{ marginTop: '1em' }}
              canBeUndefined
              label="Mois"
              month={this.state.stopMonth || ''}
              onChange={(e) => {
                this.setState((prevState) => ({
                  ...prevState,
                  stopMonth: (e?.target?.value as number) || null,
                }));
              }}
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.setStopModalExpId(null)} color="primary">
            Annuler
          </Button>
          <DatabaseClient>
            {(db) => {
              const onClick = () => {
                db.writeExpectation(this.state.stopModal || '', {
                  ...db.expectations[this.state.stopModal || ''],
                  endMonth: this.state.stopMonth,
                  endYear: this.state.stopYear,
                });
                this.setStopModalExpId(null);
              };
              return (
                <Button onClick={onClick} color="primary">
                  Valider
                </Button>
              );
            }}
          </DatabaseClient>
        </DialogActions>
      </Dialog>
    ) : null;
  };

  renderRemoveModal = () => {
    return this.state.removeModal !== null ? (
      <Dialog
        open
        onClose={() => this.setRemoveModalExpId(null)}
        aria-labelledby="dialog-remove-charge-title"
        aria-describedby="dialog-remove-charge-desc"
      >
        <DialogTitle id="dialog-remove-charge-title">Supprimer une ligne ?</DialogTitle>
        <DialogContent>
          <DialogContentText id="dialog-remove-charge-desc">
            La suppression ne changera pas les mouvements réels.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.setRemoveModalExpId(null)} color="primary">
            Annuler
          </Button>
          <DatabaseClient>
            {(db) => {
              const onClick = () => {
                this.setRemoveModalExpId(null);
                db.writeExpectation(this.state.removeModal || '', null);
              };
              return (
                <Button onClick={onClick} color="primary">
                  Supprimer
                </Button>
              );
            }}
          </DatabaseClient>
        </DialogActions>
      </Dialog>
    ) : null;
  };

  renderRenameModal = () => {
    return this.state.renameModal !== null ? (
      <Dialog open onClose={() => this.setRenameModalExpId(null)} aria-labelledby="dialog-stop-charge-title">
        <DialogTitle id="dialog-stop-charge-title">Renomer la ligne</DialogTitle>
        <DialogContent>
          <Grid container direction="column">
            <DatabaseClient>
              {(db) => {
                return (
                  <TextField
                    value={this.state.name || db.expectations[this.state.renameModal || ''].label}
                    onChange={(e) => {
                      const label = (e?.target?.value as string) || null;
                      this.setState((prevState) => ({
                        ...prevState,
                        name: label,
                      }));
                    }}
                  />
                );
              }}
            </DatabaseClient>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.setRenameModalExpId(null)} color="primary">
            Annuler
          </Button>
          <DatabaseClient>
            {(db) => {
              const onClick = () => {
                db.writeExpectation(this.state.renameModal || '', {
                  ...db.expectations[this.state.renameModal || ''],
                  label: this.state.name || '',
                });
                this.setRenameModalExpId(null);
              };
              return (
                <Button onClick={onClick} color="primary">
                  Valider
                </Button>
              );
            }}
          </DatabaseClient>
        </DialogActions>
      </Dialog>
    ) : null;
  };

  render() {
    const btnStyle: React.CSSProperties = {
      marginTop: '1em',
      float: 'right',
    };

    return (
      <Container>
        <Typography variant="h1">Prévisions</Typography>
        <DateConsumer>
          {(date) => (
            <React.Fragment>
              <DateSelector
                month={date.month}
                year={date.year}
                onMonthChange={date.setMonth}
                onYearChange={date.setYear}
              />
              <ExpectationsList
                style={{ marginTop: '1em' }}
                setStopModalExpId={this.setStopModalExpId}
                setRemoveModalExpId={this.setRemoveModalExpId}
                setRenameModalExpId={this.setRenameModalExpId}
                year={date.year}
                month={date.month}
              />
            </React.Fragment>
          )}
        </DateConsumer>
        <Button variant="contained" color="primary" style={btnStyle} component={Link} to="/forecasts/new">
          Ajouter
        </Button>

        {this.renderStopModal()}
        {this.renderRemoveModal()}
        {this.renderRenameModal()}
      </Container>
    );
  }
}

export default Forecasts;
