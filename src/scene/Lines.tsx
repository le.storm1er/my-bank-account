import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { DatabaseClient } from '../component/Database';
import DateSelector from '../component/DateSelector';
import LinesList from '../component/LinesList';
import { DateConsumer } from '../component/DateProvider';

type LinesState = {
  removeModal: string | null;
};

class Lines extends React.Component<{}, LinesState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      removeModal: null,
    };
  }

  setRemoveModalLineId = (uri: string | null) => {
    this.setState((prevState) => ({
      ...prevState,
      removeModal: uri,
    }));
  };

  renderRemoveModal = () => {
    return this.state.removeModal !== null ? (
      <Dialog
        open
        onClose={() => this.setRemoveModalLineId(null)}
        aria-labelledby="dialog-remove-charge-title"
        aria-describedby="dialog-remove-charge-desc"
      >
        <DialogTitle id="dialog-remove-charge-title">Supprimer une ligne ?</DialogTitle>
        <DialogContent>
          <DialogContentText id="dialog-remove-charge-desc">
            La suppression ne changera pas les prévisions.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.setRemoveModalLineId(null)} color="primary">
            Annuler
          </Button>
          <DatabaseClient>
            {(db) => {
              const onClick = () => {
                this.setRemoveModalLineId(null);
                db.writeLine(this.state.removeModal || '', null);
              };
              return (
                <Button onClick={onClick} color="primary">
                  Supprimer
                </Button>
              );
            }}
          </DatabaseClient>
        </DialogActions>
      </Dialog>
    ) : null;
  };

  render() {
    const spacingStyle = { marginTop: '1em' };
    return (
      <Container>
        <Typography variant="h1">Charges &amp; revenus</Typography>
        <DateConsumer>
          {(date) => (
            <React.Fragment>
              <DateSelector
                month={date.month}
                year={date.year}
                onMonthChange={date.setMonth}
                onYearChange={date.setYear}
              />
              <LinesList
                style={spacingStyle}
                year={date.year}
                month={date.month}
                setRemoveModalLineId={this.setRemoveModalLineId}
              />
              <Button
                variant="contained"
                color="primary"
                style={{ ...spacingStyle, float: 'right' }}
                component={Link}
                to="/lines/new"
              >
                Ajouter une ligne
              </Button>

              {this.renderRemoveModal()}
            </React.Fragment>
          )}
        </DateConsumer>
      </Container>
    );
  }
}

export default Lines;
