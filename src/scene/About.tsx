import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';

class About extends React.Component {
  render() {
    return (
      <Container>
        <Typography variant="h1">À propos</Typography>
        <Typography variant="h2">Mentions Légales</Typography>
        <Typography variant="body1">
          Icône faites par{' '}
          <a href="https://www.flaticon.com/authors/freepik" title="Freepik" rel="noopener noreferrer" target="_blank">
            Freepik
          </a>{' '}
          sur{' '}
          <a href="https://www.flaticon.com/" title="Flaticon">
            {' '}
            www.flaticon.com
          </a>
        </Typography>
        <Typography variant="body1">
          Code source accessible{' '}
          <a
            href="https://gitlab.com/le.storm1er/my-bank-account"
            title="Gitlab"
            rel="noopener noreferrer"
            target="_blank"
          >
            sur GitLab
          </a>
          .
        </Typography>
        <Typography variant="h2">Confidentialité</Typography>
        <Typography variant="body1">
          Toutes les données sont conservé localement dans votre navigateur, à l'exception de la synchronisation via
          Google Drive.
          <br />
          <a href="https://support.google.com/drive/answer/2450387?hl=fr" rel="noopener noreferrer" target="_blank">
            Voir les conditions d'usage de Google Drive
          </a>
        </Typography>
        <Typography variant="h2">Conditions d'usage</Typography>
        <Typography variant="body1">
          Aucune conditions d'usage particulière applicable, à utiliser selon les lois en vigueur dans votre pays.
        </Typography>
      </Container>
    );
  }
}

export default About;
