import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import { DatabaseState } from '../component/Database';
import { GoogleSyncClient, GoogleSyncState } from '../component/GoogleSync';
import NewSpreadSheetDialog from '../component/NewSpreadSheetDialog';
import { SelectSpreadSheetDialog } from './../component/SelectSpreadSheetDialog';

type SyncState = {
  selectSpreadSheetModal: boolean;
  newSpreadSheetModal: boolean;
  newFileName: string;
};

class Sync extends React.Component<{}, SyncState> {
  state = {
    selectSpreadSheetModal: false,
    newSpreadSheetModal: false,
    newFileName: '',
  };

  constructor(props: {}) {
    super(props);

    this.closeSelectSpreadSheetModal = this.closeSelectSpreadSheetModal.bind(this);
    this.openNewSpreadSheetModal = this.openNewSpreadSheetModal.bind(this);
    this.selectSpreadSheet = this.selectSpreadSheet.bind(this);
    this.closeNewSpreadSheetModal = this.closeNewSpreadSheetModal.bind(this);
    this.createNewSpreadSheetModal = this.createNewSpreadSheetModal.bind(this);
  }

  openSelectSpreadSheetModal(gs: GoogleSyncState): () => void {
    return () => {
      gs.updateFileList();
      this.setState((prevState) => ({
        ...prevState,
        selectSpreadSheetModal: true,
      }));
    };
  }

  closeSelectSpreadSheetModal() {
    this.setState((prevState) => ({
      ...prevState,
      selectSpreadSheetModal: false,
    }));
  }

  openNewSpreadSheetModal(): void {
    this.closeSelectSpreadSheetModal();
    this.setState((prevState) => ({ ...prevState, newSpreadSheetModal: true }));
  }

  closeNewSpreadSheetModal(): void {
    this.setState((prevState) => ({ ...prevState, newSpreadSheetModal: false, newFileName: '' }));
  }

  selectSpreadSheet(gs: GoogleSyncState, id: string): () => void {
    return () => {
      gs.setSpreadSheet(id);
      this.closeSelectSpreadSheetModal();
    };
  }

  createNewSpreadSheetModal(gs: GoogleSyncState, db: DatabaseState): () => void {
    return () => {
      gs.createSpreadSheet(this.state.newFileName, db);
      this.closeNewSpreadSheetModal();
    };
  }

  render() {
    const btnSpaces: React.CSSProperties = {
      marginTop: '1em',
      marginBottom: '1em',
    };
    return (
      <Container style={{ paddingBottom: '1em' }}>
        <Typography variant="h1">Synchroniser</Typography>
        <Card>
          <CardContent>
            <Grid container direction="column" justify="center" alignItems="center" alignContent="center">
              <Typography variant="body1">
                Vous pouvez activer la synchronisation via Google Drive. Cela créera un fichier Google Sheets contenant
                les données de l'application. À chaque mise à jour dans l'application, le fichier sera modifié en
                conséquence.
              </Typography>
              <GoogleSyncClient>
                {(gs) =>
                  gs.status === 'offline' ? (
                    <Typography variant="body2">
                      Cette fonctionnalité n'est pas disponible hors ligne. Vous devez être connecté à internet pour
                      utiliser la synchronisation.
                    </Typography>
                  ) : gs.status === 'init' ? (
                    <CircularProgress variant="indeterminate" style={btnSpaces} />
                  ) : !gs.isSignedIn ? (
                    <Button style={btnSpaces} color="primary" variant="contained" onClick={gs.requestSignIn}>
                      Connexion
                    </Button>
                  ) : (
                    <React.Fragment>
                      <Button style={btnSpaces} color="primary" variant="contained" onClick={gs.requestSignOut}>
                        Déconnexion
                      </Button>
                      {!gs.spreadSheetId ? (
                        <Typography variant="body1">
                          Vous devez créer ou sélectionner un fichier avec lequel vous synchroniser.
                        </Typography>
                      ) : null}
                      <Button
                        style={btnSpaces}
                        color="primary"
                        variant="contained"
                        onClick={this.openSelectSpreadSheetModal(gs)}
                      >
                        {gs.spreadSheetId ? 'Changer de fichier' : 'Sélectionner un fichier'}
                      </Button>
                    </React.Fragment>
                  )
                }
              </GoogleSyncClient>
            </Grid>
          </CardContent>
        </Card>
        {this.renderSelectSpreadSheetModal()}
        {this.renderNewSpreadSheetModal()}
      </Container>
    );
  }

  renderSelectSpreadSheetModal(): React.ReactNode {
    return this.state.selectSpreadSheetModal ? (
      <SelectSpreadSheetDialog
        closeSelectSpreadSheetModal={this.closeSelectSpreadSheetModal}
        openNewSpreadSheetModal={this.openNewSpreadSheetModal}
        selectSpreadSheet={this.selectSpreadSheet}
      />
    ) : null;
  }

  renderNewSpreadSheetModal(): React.ReactNode {
    return this.state.newSpreadSheetModal ? (
      <NewSpreadSheetDialog
        closeNewSpreadSheetModal={this.closeNewSpreadSheetModal}
        createNewSpreadSheetModal={this.createNewSpreadSheetModal}
        name={this.state.newFileName}
        onChange={(e) => {
          const newFileName = e?.target?.value || '';
          this.setState((prevState) => {
            return { ...prevState, newFileName };
          });
        }}
      />
    ) : null;
  }
}

export default Sync;
