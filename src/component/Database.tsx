import * as React from 'react';
import { updateCurrentAndNextMonth } from '../util/updateCurrentAndNextMonth';
import valueRangesToDatabaseData from '../util/valueRangesToDatabaseData';

export type Line = {
  label: string;
  amount: number;
  year: number;
  month: number;
  expectation: string | null;
};

export type Expectation = {
  label: string;
  amount: number;
  startYear: number;
  startMonth: number;
  endYear: number | null;
  endMonth: number | null;
  lines: Array<string>;
};

export type MonthState = {
  year: number;
  month: number;
  realStartAmount: number;
  realMovement: number;
  expectedMovement: number;
};

export type DatabaseData = {
  lines: Record<string, Line>;
  expectations: Record<string, Expectation>;
  monthStates: Record<string, MonthState>;
};

export type DatabaseState = DatabaseData & {
  writeLine(uri: string, line: Line | null, callback?: () => void): void;
  writeExpectation(uri: string, expectation: Expectation | null): void;
  writeMonthState(monthState: MonthState): void;
  ready: boolean;
};

export type DatabaseProps = {
  children: JSX.Element;
  onUpdate?: (db: DatabaseState) => void;
  onImport?: (callback: () => void) => void;
  importedData?: gapi.client.sheets.ValueRange[];
  syncState: 'init' | 'import' | 'export' | 'waiting' | 'offline';
};

function isDatabaseComponent(el: any): el is Database {
  if (typeof el === 'undefined' || typeof el.setState !== 'function') {
    throw new Error('You must bind this function to the Database component to use it.');
  }
  return true;
}

const defaultDatabaseContext: DatabaseState = {
  expectations: {},
  lines: {},
  monthStates: {},
  ready: true,
  writeLine(uri: string, line: Line | null, callback?: () => void) {
    if (!isDatabaseComponent(this)) return;
    let _this = this;
    this.setState(
      (prevState) => {
        // Removing the line from every expectation.
        for (const expUri in prevState.expectations) {
          prevState.expectations[expUri].lines = prevState.expectations[expUri].lines.filter(
            (linkUri) => linkUri !== uri,
          );
        }

        if (line === null) {
          const year = prevState.lines[uri].year;
          const month = prevState.lines[uri].month;
          delete prevState.lines[uri];
          return updateCurrentAndNextMonth(year, month, prevState);
        }

        // Adding the line from expectation if exist.
        if (prevState.expectations[line.expectation || -1]) {
          prevState.expectations[line.expectation || -1].lines.push(uri);
        }

        return updateCurrentAndNextMonth(line.year, line.month, {
          ...prevState,
          lines: {
            ...prevState.lines,
            [uri]: line,
          },
        });
      },
      () => {
        if (_this.props.onUpdate) {
          _this.props.onUpdate(_this.state);
          if (callback) {
            callback();
          }
        }
      },
    );
  },

  writeExpectation(uri: string, expectation: Expectation | null) {
    if (!isDatabaseComponent(this)) return;
    let _this = this;
    this.setState(
      (prevState) => {
        if (expectation === null) {
          const year = prevState.expectations[uri].startYear;
          const month = prevState.expectations[uri].startMonth;
          delete prevState.expectations[uri];

          // Ensure lines doesn't have this expectation for link.
          for (const lineUri in prevState.lines) {
            prevState.lines[lineUri].expectation =
              prevState.lines[lineUri].expectation === uri ? null : prevState.lines[lineUri].expectation;
          }

          return updateCurrentAndNextMonth(year, month, prevState);
        }

        return updateCurrentAndNextMonth(expectation.startYear, expectation.startMonth, {
          ...prevState,
          expectations: {
            ...prevState.expectations,
            [uri]: expectation,
          },
        });
      },
      () => {
        if (_this.props.onUpdate) {
          _this.props.onUpdate(_this.state);
        }
      },
    );
  },

  writeMonthState(monthState: MonthState) {
    if (!isDatabaseComponent(this)) return;
    let _this = this;
    const uri = `${monthState.year}/${monthState.month}`;
    // TODO: handle monthStateUpdate;
    this.setState(
      (prevState) =>
        updateCurrentAndNextMonth(monthState.year, monthState.month, {
          ...prevState,
          monthStates: {
            ...prevState.monthStates,
            [uri]: monthState,
          },
        }),
      () => {
        if (_this.props.onUpdate) {
          _this.props.onUpdate(_this.state);
        }
      },
    );
  },
};

const DatabaseContext = React.createContext(defaultDatabaseContext);
const DatabaseClient = DatabaseContext.Consumer;

class Database extends React.Component<DatabaseProps, DatabaseState> {
  constructor(props: DatabaseProps) {
    super(props);

    let linesStored = localStorage.getItem('lines');
    let expectationsStored = localStorage.getItem('expectations');
    let monthStateStored = localStorage.getItem('monthStates');
    let lines: Record<string, Line> = {};
    let expectations: Record<string, Expectation> = {};
    let monthStates: Record<string, MonthState> = {};

    if (linesStored !== null) {
      try {
        lines = JSON.parse(linesStored);
      } catch (error) {}
    }
    if (expectationsStored !== null) {
      try {
        expectations = JSON.parse(expectationsStored);
      } catch (error) {}
    }
    if (monthStateStored !== null) {
      try {
        monthStates = JSON.parse(monthStateStored);
      } catch (error) {}
    }

    this.state = {
      ...defaultDatabaseContext,
      lines,
      expectations,
      monthStates,
      writeLine: defaultDatabaseContext.writeLine.bind(this),
      writeExpectation: defaultDatabaseContext.writeExpectation.bind(this),
      writeMonthState: defaultDatabaseContext.writeMonthState.bind(this),
    };
  }

  componentDidMount() {
    if (Object.keys(this.state.monthStates).length === 0) {
      const today = new Date();
      let newMonth: MonthState = {
        expectedMovement: 0,
        month: today.getMonth(),
        year: today.getFullYear(),
        realMovement: 0,
        realStartAmount: 0,
      };
      this.state.writeMonthState(newMonth);
    }
  }

  componentDidUpdate() {
    localStorage.setItem('lines', JSON.stringify(this.state.lines));
    localStorage.setItem('expectations', JSON.stringify(this.state.expectations));
    localStorage.setItem('monthStates', JSON.stringify(this.state.monthStates));

    if (this.props.syncState === 'import' && this.props.importedData) {
      const dataToImport = this.props.importedData;
      this.setState(
        (prevState) => ({
          ...prevState,
          ready: false,
        }),
        () => {
          this.importData(dataToImport);
        },
      );
    }
  }

  importData(dataToImport: gapi.client.sheets.ValueRange[]) {
    const newState = valueRangesToDatabaseData(dataToImport);
    if (!newState) {
      // TODO: feedback on import fail
    } else {
      this.props.onImport &&
        this.props.onImport(() => {
          this.setState((prevState) => ({
            ...prevState,
            ...newState,
            ready: true,
          }));
        });
    }
  }

  render() {
    return <DatabaseContext.Provider value={this.state}>{this.props.children}</DatabaseContext.Provider>;
  }
}

export default Database;
export { DatabaseClient };
