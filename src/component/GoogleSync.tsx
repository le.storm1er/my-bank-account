import * as React from 'react';
import { DatabaseState, Expectation, Line, MonthState } from './Database';

const SCOPE = 'https://www.googleapis.com/auth/drive.file';
export const LinesRanges = "'Lignes'!A1:F999999";
export const ExpecRanges = "'Prévisions'!A1:G999999";
export const MonthRanges = "'Résumés'!A1:F999999";

type GoogleSyncProps = {
  children: JSX.Element;
};

export type GoogleSyncState = {
  spreadSheetId: string | null;
  isSignedIn: boolean;
  status: 'init' | 'import' | 'export' | 'waiting' | 'offline';
  fileList: Array<{ id: string; name: string }> | null;
  importedData?: gapi.client.sheets.ValueRange[];
  conflict: boolean;
  createSpreadSheet(name: string, db: DatabaseState): void;
  updateFileList(): void;
  setSpreadSheet(spreadSheetId: string | null): void;
  requestSignIn(): void;
  requestSignOut(): void;
  requestExport(db: DatabaseState): void;
  onceImported(callback: () => void): void;
  requestImport(): void;
};

function isGoogleSyncComponent(el: any): el is GoogleSync {
  if (typeof el === 'undefined' || typeof el.setState !== 'function') {
    throw new Error('You must bind this function to the Google component to use it.');
  }
  return true;
}

const defaultGoogleSyncContext: GoogleSyncState = {
  spreadSheetId: null,
  isSignedIn: false,
  status: 'init',
  fileList: null,
  importedData: undefined,
  conflict: false,
  createSpreadSheet(name: string, db: DatabaseState): void {
    if (!isGoogleSyncComponent(this)) return;
    let _this = this;
    this.setState(
      (prevState) => ({
        ...prevState,
        status: 'export',
      }),
      () => {
        gapi.client.sheets.spreadsheets
          .create({
            resource: {
              properties: {
                title: name,
              },
            },
            fields: 'spreadsheetId',
          })
          .then((response) => {
            if (response.status !== 200) {
              throw new Error(response.body);
            }
            return JSON.parse(response.body).spreadsheetId as string;
          })
          .then((spreadsheetId) => {
            return _this.createSheetsTabs(spreadsheetId).then(() => {
              _this.setState(
                (prevState) => ({
                  ...prevState,
                  spreadSheetId: spreadsheetId,
                  status: 'waiting',
                }),
                () => {
                  _this.state.requestExport(db);
                },
              );
            });
          })
          .catch((err) => {
            _this.setState((prevState) => ({
              ...prevState,
              status: 'waiting',
            }));
          });
      },
    );
  },
  updateFileList() {
    if (!isGoogleSyncComponent(this)) return;
    let _this = this;

    this.setState(
      (prevState) => ({
        ...prevState,
        fileList: null,
      }),
      () => {
        gapi.client.drive.files
          .list({
            q: "mimeType='application/vnd.google-apps.spreadsheet'",
          })
          .then((response) => {
            if (response.status !== 200) {
              throw new Error(response.body);
            }
            return JSON.parse(response.body);
          })
          .then((body) => {
            _this.setState((prevState: GoogleSyncState) => ({
              ...prevState,
              fileList: body.files as Array<{ id: string; name: string }>,
            }));
          })
          .catch((err) => {
            console.warn(err);
            _this.setState((prevState: GoogleSyncState) => ({
              ...prevState,
              fileList: [],
            }));
          });
      },
    );
  },
  setSpreadSheet(spreadSheetId: string | null) {
    if (!isGoogleSyncComponent(this)) return;
    this.setState((prevState) => ({
      ...prevState,
      spreadSheetId,
      conflict: true,
    }));
  },
  requestSignIn() {
    if (!isGoogleSyncComponent(this)) return;
    if (this.state.isSignedIn) return;
    const gAuth = gapi.auth2.getAuthInstance();
    gAuth.signIn();
  },
  requestSignOut() {
    if (!isGoogleSyncComponent(this)) return;
    if (!this.state.isSignedIn) return;
    const gAuth = gapi.auth2.getAuthInstance();
    gAuth.signOut();
    this.setState((prevState) => ({
      ...prevState,
      isSignedIn: false,
      spreadSheetId: null,
      status: 'waiting',
    }));
  },
  requestExport(db: DatabaseState) {
    if (!isGoogleSyncComponent(this)) return;
    let _this = this;
    if (this.state.status === 'waiting' && this.state.isSignedIn && this.state.spreadSheetId) {
      this.setState(
        (prevState) => ({
          ...prevState,
          status: 'export',
        }),
        () => {
          _this.exportToSheets(db);
        },
      );
    }
  },
  onceImported(callback: () => void): void {
    if (!isGoogleSyncComponent(this)) return;
    localStorage.removeItem('hasBeenOffline');
    this.setState((prevState) => ({ ...prevState, status: 'waiting', conflict: false }), callback);
  },
  requestImport(): void {
    if (!isGoogleSyncComponent(this)) return;
    this.importData();
  },
};

const GoogleSyncContext = React.createContext(defaultGoogleSyncContext);
const GoogleSyncClient = GoogleSyncContext.Consumer;

class GoogleSync extends React.Component<GoogleSyncProps, GoogleSyncState> {
  constructor(props: GoogleSyncProps) {
    super(props);

    this.state = {
      ...defaultGoogleSyncContext,
      spreadSheetId: localStorage.getItem('spreadSheetId'),
      createSpreadSheet: defaultGoogleSyncContext.createSpreadSheet.bind(this),
      updateFileList: defaultGoogleSyncContext.updateFileList.bind(this),
      setSpreadSheet: defaultGoogleSyncContext.setSpreadSheet.bind(this),
      requestSignIn: defaultGoogleSyncContext.requestSignIn.bind(this),
      requestSignOut: defaultGoogleSyncContext.requestSignOut.bind(this),
      requestExport: defaultGoogleSyncContext.requestExport.bind(this),
      onceImported: defaultGoogleSyncContext.onceImported.bind(this),
      requestImport: defaultGoogleSyncContext.requestImport.bind(this),
    };
  }

  createSheetsTabs(spreadSheetId: string): Promise<void> {
    const body: gapi.client.sheets.BatchUpdateSpreadsheetRequest = {
      requests: [
        {
          addSheet: {
            properties: {
              title: 'Résumés',
              tabColor: {
                red: 1,
                green: 1,
                blue: 1,
              },
            },
          },
        },
        {
          addSheet: {
            properties: {
              title: 'Prévisions',
              tabColor: {
                red: 0.7,
                green: 0.7,
                blue: 1,
              },
            },
          },
        },
        {
          addSheet: {
            properties: {
              title: 'Lignes',
              tabColor: {
                red: 0.4,
                green: 0.4,
                blue: 1,
              },
            },
          },
        },
      ],
    };
    return gapi.client.sheets.spreadsheets
      .batchUpdate(
        {
          spreadsheetId: spreadSheetId,
        },
        body,
      )
      .then((response) => {
        if (response.status !== 200) {
          throw new Error(response.body);
        }
        return JSON.parse(response.body);
      });
  }

  storeSpreadSheetId() {
    localStorage.removeItem('spreadSheetId');
    if (this.state.spreadSheetId) {
      localStorage.setItem('spreadSheetId', this.state.spreadSheetId);
    }
  }

  initGoogleApi() {
    try {
      gapi.load('client:auth2', () => {
        let discoveryUrls = [
          'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest',
          'https://sheets.googleapis.com/$discovery/rest?version=v4',
        ];
        gapi.client
          .init({
            apiKey: process.env.REACT_APP_GOOGLE_API_KEY,
            clientId: process.env.REACT_APP_GOOGLE_CLIENT_ID,
            discoveryDocs: discoveryUrls,
            scope: SCOPE,
          })
          .then(() => {
            gapi.auth2.getAuthInstance().isSignedIn.listen(this.onSigninStatusChange.bind(this));
            // Handle initial sign-in state. (Determine if user is already signed in.)
            this.onSigninStatusChange();
          })
          .catch(() => {
            this.setState((prevState) => ({ ...prevState, status: 'offline' }));
            localStorage.setItem('hasBeenOffline', 'yes');
          });
      });
    } catch (error) {
      this.setState((prevState) => ({ ...prevState, status: 'offline' }));
      localStorage.setItem('hasBeenOffline', 'yes');
    }
  }

  onSigninStatusChange() {
    const gAuth = gapi.auth2.getAuthInstance();
    if (!gAuth) {
      console.warn('gAuth is null!');
      return;
    }
    let user = gAuth.currentUser.get();
    let isAuthorized = user.hasGrantedScopes(SCOPE);
    let conflict = false;

    if (this.state.spreadSheetId && isAuthorized && this.state.status === 'init') {
      if (!localStorage.getItem('hasBeenOffline')) {
        this.importData();
      } else {
        conflict = true;
      }
    }

    this.setState((prevState) => ({
      ...prevState,
      isSignedIn: isAuthorized,
      status: 'waiting',
      conflict,
    }));
  }

  importData() {
    const _this = this;

    this.setState(
      (prevState) => ({
        ...prevState,
        status: 'import',
      }),
      () => {
        try {
          gapi.client.sheets.spreadsheets.values
            .batchGet({
              spreadsheetId: this.state.spreadSheetId || '',
              ranges: [LinesRanges, ExpecRanges, MonthRanges],
            })
            .then((response) => {
              if (response.status !== 200) {
                throw new Error(response.body);
              }
              return response.result;
            })
            .then((data) => {
              _this.setState((prevState) => ({
                ...prevState,
                status: 'import',
                importedData: data.valueRanges,
              }));
            });
        } catch (error) {
          console.warn('Unable to import data ?');
        }
      },
    );
  }

  componentDidMount() {
    this.initGoogleApi();
  }

  componentDidUpdate() {
    this.storeSpreadSheetId();
  }

  createBodyLines(lines: Record<string, Line>): gapi.client.sheets.ValueRange {
    return {
      range: LinesRanges,
      values: [
        ['ID ligne', 'Année', 'Mois', 'Label', 'Montant', 'ID prévision'],
        ...Object.keys(lines).map((lineUri) => {
          const line = lines[lineUri];
          return [lineUri, line.year, line.month, line.label, line.amount, line.expectation || ''];
        }),
        ['', '', '', '', '', ''],
      ],
      majorDimension: 'ROWS',
    };
  }
  createBodyExpectations(expectations: Record<string, Expectation>): gapi.client.sheets.ValueRange {
    return {
      range: ExpecRanges,
      values: [
        ['ID', 'Début: Année', 'Début: Mois', 'Fin: Année', 'Fin: Mois', 'Label', 'Montant'],
        ...Object.keys(expectations).map((expUri) => {
          const expectation = expectations[expUri];
          return [
            expUri,
            expectation.startYear,
            expectation.startMonth,
            expectation.endYear,
            expectation.endMonth,
            expectation.label,
            expectation.amount,
          ];
        }),
      ],
      majorDimension: 'ROWS',
    };
  }
  createBodyMonthState(monthStates: Record<string, MonthState>): gapi.client.sheets.ValueRange {
    return {
      range: MonthRanges,
      values: [
        ['ID', 'Année', 'Mois', 'Début de mois', 'Mouvements attendus', 'Mouvements réels'],
        ...Object.keys(monthStates).map((monthStateUri) => {
          const monthState = monthStates[monthStateUri];
          return [
            monthStateUri,
            monthState.year,
            monthState.month,
            monthState.realStartAmount,
            monthState.expectedMovement,
            monthState.realMovement,
          ];
        }),
      ],
      majorDimension: 'ROWS',
    };
  }
  updateSheets(bodyLines: gapi.client.sheets.ValueRange) {
    return gapi.client.sheets.spreadsheets.values.update(
      {
        spreadsheetId: this.state.spreadSheetId || '',
        range: bodyLines.range || '',
        valueInputOption: 'USER_ENTERED',
      },
      bodyLines,
    );
  }
  exportToSheets(db: DatabaseState) {
    const bodyLines = this.createBodyLines(db.lines);
    const bodyExpectations = this.createBodyExpectations(db.expectations);
    const bodyMonthState = this.createBodyMonthState(db.monthStates);
    Promise.all([this.updateSheets(bodyLines), this.updateSheets(bodyExpectations), this.updateSheets(bodyMonthState)])
      .then((res) => {
        let pass = true;
        for (let index = 0; index < res.length; index++) {
          const response = res[index];
          if (response.status !== 200) {
            pass = false;
          }
        }
        console.log(pass ? 'Export succeded' : 'Export failed');
        // TODO: feedback on failed.
        localStorage.removeItem('hasBeenOffline');
        this.setState((prevState) => ({
          ...prevState,
          status: 'waiting',
          conflict: false,
        }));
      })
      .catch((err) => {
        console.warn('Export failed', err);
        // TODO: feedback on failed.
      });
  }

  render() {
    return <GoogleSyncContext.Provider value={this.state}>{this.props.children}</GoogleSyncContext.Provider>;
  }
}

export default GoogleSync;
export { GoogleSyncClient };
