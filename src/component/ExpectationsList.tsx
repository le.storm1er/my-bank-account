import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import StopIcon from '@material-ui/icons/Stop';
import * as React from 'react';
import { ExpectationKeysInGivenMonth } from '../util/ExpectationInGivenMonth';
import MoneyFormatter from '../util/MoneyFormatter';
import { DatabaseClient } from './Database';

type ExpectationsListProps = {
  setStopModalExpId(uri: string | null): void;
  setRemoveModalExpId(uri: string | null): void;
  setRenameModalExpId(uri: string | null): void;
  year: number;
  month: number;
  style?: React.CSSProperties;
};

export default function ExpectationsList(props: ExpectationsListProps) {
  return (
    <TableContainer component={Paper} style={{ ...props.style, maxWidth: 'calc(100vw - 32px)', overflowX: 'auto' }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="left">Label</TableCell>
            <TableCell align="right">Montant</TableCell>
            <TableCell align="left">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <DatabaseClient>
            {(db) => {
              return Object.keys(db.expectations)
                .filter(ExpectationKeysInGivenMonth(props, db))
                .map((expUri) => {
                  const exp = db.expectations[expUri];
                  return (
                    <TableRow key={expUri}>
                      <TableCell align="left">{exp.label}</TableCell>
                      <TableCell align="right">{MoneyFormatter.format(exp.amount)}</TableCell>
                      <TableCell align="left">
                        <Typography noWrap>
                          <IconButton aria-label="stop" onClick={() => props.setStopModalExpId(expUri)}>
                            <StopIcon fontSize="small" />
                          </IconButton>
                          <IconButton aria-label="delete" onClick={() => props.setRemoveModalExpId(expUri)}>
                            <DeleteIcon fontSize="small" />
                          </IconButton>
                          <IconButton aria-label="rename" onClick={() => props.setRenameModalExpId(expUri)}>
                            <EditIcon fontSize="small" />
                          </IconButton>
                        </Typography>
                      </TableCell>
                    </TableRow>
                  );
                });
            }}
          </DatabaseClient>
        </TableBody>
      </Table>
    </TableContainer>
  );
}
