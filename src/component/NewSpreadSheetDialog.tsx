import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import * as React from 'react';
import { DatabaseClient, DatabaseState } from '../component/Database';
import { GoogleSyncClient, GoogleSyncState } from '../component/GoogleSync';

type NewSpreadSheetDialogProps = {
  onChange(event: React.ChangeEvent<HTMLInputElement>): void;
  name: string;
  closeNewSpreadSheetModal(): void;
  createNewSpreadSheetModal(gs: GoogleSyncState, db: DatabaseState): () => void;
};

export default function NewSpreadSheetDialog(props: NewSpreadSheetDialogProps) {
  return (
    <Dialog open onClose={props.closeNewSpreadSheetModal} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Créer un fichier</DialogTitle>
      <DialogContent>
        <DialogContentText>Choisissez un nom pour le nouveau fichier.</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          label="Nom du fichier"
          fullWidth
          value={props.name}
          onChange={props.onChange}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.closeNewSpreadSheetModal} color="secondary">
          Annuler
        </Button>
        <DatabaseClient>
          {(db) => (
            <GoogleSyncClient>
              {(gs) => (
                <Button onClick={props.createNewSpreadSheetModal(gs, db)} color="primary">
                  Créer
                </Button>
              )}
            </GoogleSyncClient>
          )}
        </DatabaseClient>
      </DialogActions>
    </Dialog>
  );
}
