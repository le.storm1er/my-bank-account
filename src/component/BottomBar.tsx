import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import ChargesIcon from '@material-ui/icons/AttachMoney';
import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/Settings';
import ForecastIcon from '@material-ui/icons/Update';
import * as React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { theme } from '../util/theme';

function BottomBar() {
  const location = useLocation();
  return (
    <BottomNavigation
      style={{
        width: 'calc(100vw)',
        overflowX: 'auto',
        backgroundColor: theme.palette.background.paper,
      }}
      value={`/${location.pathname.split('/')[1]}`}
    >
      <BottomNavigationAction
        component={Link}
        label="Prévisions"
        to="/forecasts"
        value="/forecasts"
        icon={<ForecastIcon />}
      />
      <BottomNavigationAction component={Link} label="Accueil" to="/" value="/" icon={<HomeIcon />} />
      <BottomNavigationAction
        component={Link}
        label="Charges&nbsp;&amp;&nbsp;revenus"
        to="/lines"
        value="/lines"
        icon={<ChargesIcon />}
      />
      <BottomNavigationAction
        component={Link}
        label="Paramètres"
        to="/settings"
        value="/settings"
        icon={<SettingsIcon />}
      />
    </BottomNavigation>
  );
}

export default BottomBar;
