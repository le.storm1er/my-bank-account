import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import * as React from 'react';
import { ExpectationKeysInGivenMonth } from '../util/ExpectationInGivenMonth';
import { ExpectationRealAmountForGivenMonth } from '../util/ExpectationRealAmountForGivenMonth';
import MoneyFormatter from '../util/MoneyFormatter';
import { DatabaseClient } from './Database';
import { CircularProgress, Grid, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { uuid } from 'uuidv4';
import { useHistory } from 'react-router-dom';
import { LineInGivenMonth } from '../util/LineInGivenMonth';

export default function ExpectationsTable(props: { year: number; month: number; style?: React.CSSProperties }) {
  const history = useHistory();
  return (
    <TableContainer component={Paper} style={{ ...props.style }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="left">Label</TableCell>
            <TableCell align="right">Montant</TableCell>
            <TableCell align="left">Statut</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <DatabaseClient>
            {(db) => {
              const amountOverExpectation: number = Object.keys(db.expectations)
                .filter(ExpectationKeysInGivenMonth(props, db))
                .map((expUri) => {
                  const exp = db.expectations[expUri];
                  return {
                    amount: exp.lines
                      .map((uri) => db.lines[uri])
                      .filter(LineInGivenMonth(props))
                      .reduce((accu, line) => accu + line.amount, 0.0),
                    exp: exp,
                  };
                })
                .reduce((accu, el) => {
                  let ret: string | number = 0.0;

                  if (el.exp.amount < 0 && Math.abs(el.amount) > Math.abs(el.exp.amount)) {
                    ret = Math.abs(el.amount) - Math.abs(el.exp.amount);
                  }
                  ret = ret.toFixed(2);

                  return accu - parseFloat(ret);
                }, 0.0);

              const amountWithoutExpectation = Object.values(db.lines)
                .filter(LineInGivenMonth(props))
                .filter((val) => !val.expectation)
                .reduce((accu, line) => accu + line.amount, 0.0);

              if (amountWithoutExpectation + amountOverExpectation === 0.0) {
                return null;
              }

              return (
                <TableRow>
                  <TableCell align="left">Dépassement</TableCell>
                  <TableCell align="right">
                    {MoneyFormatter.format(amountWithoutExpectation + amountOverExpectation)}
                  </TableCell>
                  <TableCell align="left">
                    <Grid container direction="row" justify="space-around" alignItems="center" alignContent="center">
                      <Fab
                        style={{
                          position: 'relative',
                          width: '40px',
                          height: '40px',
                          color: 'black',
                          backgroundColor: 'white',
                        }}
                        size="small"
                        onClick={() => {
                          history.push('/lines/new');
                        }}
                      >
                        <CircularProgress
                          style={{
                            position: 'absolute',
                            color: 'red',
                          }}
                          variant="static"
                          value={100}
                        />
                        <AddIcon
                          style={{
                            position: 'absolute',
                          }}
                        />
                      </Fab>
                    </Grid>
                  </TableCell>
                </TableRow>
              );
            }}
          </DatabaseClient>
          <DatabaseClient>
            {(db) => {
              return Object.keys(db.expectations)
                .filter(ExpectationKeysInGivenMonth(props, db))
                .map((expUri) => {
                  let exp = db.expectations[expUri];
                  let expReal = parseFloat(ExpectationRealAmountForGivenMonth(exp, db, props).toFixed(2));

                  const progress = (expReal / exp.amount) * 100;

                  const onClick = () => {
                    const newUri = uuid();
                    db.writeLine(
                      newUri,
                      {
                        amount: exp.amount,
                        expectation: expUri,
                        label: '',
                        month: props.month,
                        year: props.year,
                      },
                      () => {
                        history.push(`/lines/edit/${newUri}`);
                      },
                    );
                  };

                  return (
                    <TableRow key={`${exp.startYear}/${exp.startMonth}/${exp.label}`}>
                      <TableCell align="left">{exp.label}</TableCell>
                      <TableCell align="right">
                        {Math.abs(expReal) > Math.abs(exp.amount) ? (
                          <React.Fragment>
                            ({MoneyFormatter.format(exp.amount)})<br />
                          </React.Fragment>
                        ) : null}
                        {MoneyFormatter.format(Math.abs(expReal) > Math.abs(exp.amount) ? expReal : exp.amount)}
                      </TableCell>
                      <TableCell align="left">
                        <Grid
                          container
                          direction="row"
                          justify="space-around"
                          alignItems="center"
                          alignContent="center"
                        >
                          <Fab
                            style={{
                              position: 'relative',
                              width: '40px',
                              height: '40px',
                              color: 'black',
                              backgroundColor: 'white',
                            }}
                            size="small"
                            onClick={onClick}
                          >
                            {progress > 100 ? (
                              <CircularProgress
                                style={{
                                  position: 'absolute',
                                  color: 'lime',
                                }}
                                variant="static"
                                value={100}
                              />
                            ) : null}
                            <CircularProgress
                              style={{
                                position: 'absolute',
                                color: progress > 100 ? 'red' : 'lime',
                              }}
                              variant="static"
                              value={progress > 100 ? progress - 100 : progress}
                            />
                            <AddIcon
                              style={{
                                position: 'absolute',
                              }}
                            />
                          </Fab>
                        </Grid>
                      </TableCell>
                    </TableRow>
                  );
                });
            }}
          </DatabaseClient>
        </TableBody>
      </Table>
    </TableContainer>
  );
}
