import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import * as React from 'react';
import { uuid } from 'uuidv4';

export default function YearSelector(props: {
  canBeUndefined?: boolean;
  style?: React.CSSProperties;
  year: number | '';
  label: string;
  onChange(e: React.ChangeEvent<{ name?: string | undefined; value: unknown }>): void;
}) {
  const htmlId = uuid();
  return (
    <FormControl style={{ ...props.style }}>
      <InputLabel id={`${htmlId}-label`}>{props.label}</InputLabel>
      <Select onChange={props.onChange} labelId={`${htmlId}-label`} id={htmlId} value={props.year}>
        {props.canBeUndefined ? <MenuItem value={''}>Indéfinie</MenuItem> : null}
        <MenuItem value={2020}>2020</MenuItem>
        <MenuItem value={2021}>2021</MenuItem>
        <MenuItem value={2022}>2022</MenuItem>
      </Select>
    </FormControl>
  );
}
