import { Grid } from '@material-ui/core';
import React from 'react';
import { theme } from '../util/theme';
import splashLogo from '../static-assets/logo512.png';

export function SplashScreen() {
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      alignContent="center"
      style={{
        height: '100vh',
        backgroundColor: theme.palette.background.default,
      }}
    >
      <img
        src={splashLogo}
        alt="My Bank Account"
        style={{
          maxWidth: '50vw',
          maxHeight: '50vh',
        }}
      />
    </Grid>
  );
}
