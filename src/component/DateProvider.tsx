import * as React from 'react';

export type DateState = {
  year: number;
  month: number;
  setMonth(month: number, callback?: () => void): void;
  setYear(year: number, callback?: () => void): void;
  setYearAndMonth(year: number, month: number, callback?: () => void): void;
};

function isDateComponent(el: any): el is DateProvider {
  if (typeof el === 'undefined' || typeof el.setState !== 'function') {
    throw new Error('You must bind this function to the Date component to use it.');
  }
  return true;
}

const defaultDatabaseContext: DateState = {
  month: new Date().getMonth(),
  year: new Date().getFullYear(),
  setMonth(month: number, callback?: () => void) {
    if (!isDateComponent(this)) return;
    this.setState(
      (prevState) => ({
        ...prevState,
        month,
      }),
      () => {
        if (callback) {
          callback();
        }
      },
    );
  },
  setYear(year: number, callback?: () => void) {
    if (!isDateComponent(this)) return;
    this.setState(
      (prevState) => ({
        ...prevState,
        year,
      }),
      () => {
        if (callback) {
          callback();
        }
      },
    );
  },
  setYearAndMonth(year: number, month: number, callback?: () => void) {
    if (!isDateComponent(this)) return;
    this.setState(
      (prevState) => ({
        ...prevState,
        year,
        month,
      }),
      () => {
        if (callback) {
          callback();
        }
      },
    );
  },
};

const DateContext = React.createContext(defaultDatabaseContext);
const DateConsumer = DateContext.Consumer;

class DateProvider extends React.Component<{}, DateState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      ...defaultDatabaseContext,
      setMonth: defaultDatabaseContext.setMonth.bind(this),
      setYear: defaultDatabaseContext.setYear.bind(this),
      setYearAndMonth: defaultDatabaseContext.setYearAndMonth.bind(this),
    };
  }

  render() {
    return <DateContext.Provider value={this.state}>{this.props.children}</DateContext.Provider>;
  }
}

export default DateProvider;
export { DateConsumer };
