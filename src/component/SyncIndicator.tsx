import React from 'react';
import { Tooltip } from '@material-ui/core';
import ExportIcon from '@material-ui/icons/CloudUpload';
import OfflineIcon from '@material-ui/icons/OfflineBolt';

const tooltipTitles = {
  export: 'Exportation en cours',
  offline: "Vous utilisez l'application hors ligne",
};

export default function SyncIndicator(props: { status: 'init' | 'import' | 'export' | 'waiting' | 'offline' }) {
  // @ts-ignore
  const title: any = tooltipTitles[props.status];
  if (typeof title !== 'string') {
    return null;
  }

  return (
    <div
      style={{
        position: 'fixed',
        top: '.5em',
        right: '.5em',
      }}
    >
      <Tooltip title={title}>{props.status === 'export' ? <ExportIcon /> : <OfflineIcon />}</Tooltip>
    </div>
  );
}
