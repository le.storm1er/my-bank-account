import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import * as React from 'react';
import { DatabaseClient, MonthState } from '../component/Database';
import getPreviousMonthState from '../util/getPreviousMonthState';
import MoneyFormatter from '../util/MoneyFormatter';
import { ExpectationKeysInGivenMonth } from '../util/ExpectationInGivenMonth';
import { LineInGivenMonth } from '../util/LineInGivenMonth';

export default function MonthStateDisplay(props: { year: number; month: number; style?: React.CSSProperties }) {
  return (
    <DatabaseClient>
      {(db) => {
        const uri = `${props.year}/${props.month}`;
        let selectedMonthState: MonthState = db.monthStates[uri];
        let previousMonthState = getPreviousMonthState(db.monthStates, uri);
        if (!selectedMonthState) {
          selectedMonthState = {
            month: props.month,
            year: props.year,
            expectedMovement: 0,
            realMovement: 0,
            realStartAmount: 0,
          };
        }

        const startMonthAmountReal = selectedMonthState.realStartAmount;
        const startMonthAmountExpected = previousMonthState.realStartAmount + previousMonthState.expectedMovement;
        const movementReal = selectedMonthState.realMovement;
        const movementExpected = selectedMonthState.expectedMovement;
        const endMonthAmountReal = selectedMonthState.realStartAmount + selectedMonthState.realMovement;
        const endMonthAmountExpected = selectedMonthState.realStartAmount + selectedMonthState.expectedMovement;
        const amountOverExpectation: number = Object.keys(db.expectations)
          .filter(ExpectationKeysInGivenMonth(props, db))
          .map((expUri) => {
            const exp = db.expectations[expUri];
            return {
              amount: exp.lines
                .map((uri) => db.lines[uri])
                .filter(LineInGivenMonth(props))
                .reduce((accu, line) => accu + line.amount, 0.0),
              exp: exp,
            };
          })
          .reduce((accu, el) => {
            let ret: string | number = 0.0;

            if (el.exp.amount < 0 && Math.abs(el.amount) > Math.abs(el.exp.amount)) {
              ret = Math.abs(el.amount) - Math.abs(el.exp.amount);
            }
            ret = ret.toFixed(2);

            return accu - parseFloat(ret);
          }, 0.0);

        const amountWithoutExpectation = Object.values(db.lines)
          .filter(LineInGivenMonth(props))
          .filter((val) => !val.expectation)
          .reduce((accu, line) => accu + line.amount, 0.0);

        const amountOverhead = amountWithoutExpectation + amountOverExpectation;
        const mixedMouvement = movementExpected + amountOverhead;
        const mixedEndOfMonth = startMonthAmountReal + mixedMouvement;

        return (
          <TableContainer component={Paper} style={{ ...props.style }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Nom</TableCell>
                  <TableCell align="right">Réel</TableCell>
                  <TableCell align="right">Mixed</TableCell>
                  <TableCell align="right">Attendu</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell component="th">Début de mois</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(startMonthAmountReal)}</TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right">{MoneyFormatter.format(startMonthAmountExpected)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th">Mouvement</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(movementReal)}</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(mixedMouvement)}</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(movementExpected)}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th">Fin de mois</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(endMonthAmountReal)}</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(mixedEndOfMonth)}</TableCell>
                  <TableCell align="right">{MoneyFormatter.format(endMonthAmountExpected)}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        );
      }}
    </DatabaseClient>
  );
}
