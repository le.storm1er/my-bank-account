import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import * as React from 'react';
import MonthSelector from './MonthSelector';
import YearSelector from './YearSelector';

export default function DateSelector(props: {
  month: number;
  year: number;
  onMonthChange(month: number): void;
  onYearChange(year: number): void;
}) {
  return (
    <Card>
      <CardContent>
        <Grid container direction="row" alignContent="space-around" justify="space-around" alignItems="flex-end">
          <YearSelector
            label="Année"
            year={props.year}
            onChange={(e) => {
              props.onYearChange(e.target.value as number);
            }}
          />
          <MonthSelector
            label="Mois"
            month={props.month}
            onChange={(e) => {
              props.onMonthChange(e.target.value as number);
            }}
          />
        </Grid>
      </CardContent>
    </Card>
  );
}
