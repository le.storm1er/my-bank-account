import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import * as React from 'react';
import { Link } from 'react-router-dom';
import MoneyFormatter from '../util/MoneyFormatter';
import { DatabaseClient } from './Database';

type LinesListProps = {
  style?: React.CSSProperties;
  year: number;
  month: number;
  setRemoveModalLineId: (uri: string | null) => void;
};

type LinesListState = {};

class LinesList extends React.Component<LinesListProps, LinesListState> {
  render() {
    return (
      <TableContainer
        component={Paper}
        style={{ ...this.props.style, maxWidth: 'calc(100vw - 32px)', overflowX: 'auto' }}
      >
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="left">Prévision</TableCell>
              <TableCell align="left">Label</TableCell>
              <TableCell align="right">Montant</TableCell>
              <TableCell align="left">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <DatabaseClient>
              {(db) => {
                const currentDate = parseInt(`${this.props.year}${this.props.month}`);
                return Object.keys(db.lines)
                  .filter((lineUri) => {
                    const line = db.lines[lineUri];
                    const lineDate = parseInt(`${line.year}${line.month}`);
                    return lineDate === currentDate;
                  })
                  .map((lineUri) => {
                    const line = db.lines[lineUri];
                    const exp = db.expectations[line.expectation || ''];
                    const forecastLabel = exp ? exp.label : '';
                    return (
                      <TableRow key={lineUri}>
                        <TableCell align="left">{forecastLabel}</TableCell>
                        <TableCell align="left">{line.label}</TableCell>
                        <TableCell align="right">{MoneyFormatter.format(line.amount)}</TableCell>
                        <TableCell align="left">
                          <Typography noWrap>
                            <IconButton aria-label="edit" component={Link} to={`/lines/edit/${lineUri}`}>
                              <EditIcon fontSize="small" />
                            </IconButton>
                            <IconButton aria-label="delete" onClick={() => this.props.setRemoveModalLineId(lineUri)}>
                              <DeleteIcon fontSize="small" />
                            </IconButton>
                          </Typography>
                        </TableCell>
                      </TableRow>
                    );
                  });
              }}
            </DatabaseClient>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default LinesList;
