import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import SelectedFileIcon from '@material-ui/icons/RadioButtonChecked';
import NotSelectedFileIcon from '@material-ui/icons/RadioButtonUnchecked';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { GoogleSyncClient, GoogleSyncState } from './GoogleSync';
import { ListItemIcon } from '@material-ui/core';

type SelectSpreadSheetDialogProps = {
  closeSelectSpreadSheetModal(): void;
  openNewSpreadSheetModal(): void;
  selectSpreadSheet(gs: GoogleSyncState, spreadSheetId: string): () => void;
};

export function SelectSpreadSheetDialog(props: SelectSpreadSheetDialogProps) {
  return (
    <GoogleSyncClient>
      {(gs) => (
        <Dialog open onClose={props.closeSelectSpreadSheetModal} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Selectionner un fichier</DialogTitle>
          <DialogContent>
            <DialogContentText>Voici la liste des fichiers Sheets créées par cette application.</DialogContentText>
            {gs.fileList === null ? (
              <Grid container justify="center">
                <CircularProgress />
              </Grid>
            ) : gs.fileList.length === 0 ? (
              <Typography variant="body1" component="p">
                Aucun fichier disponible, vous pouvez en créer un ci dessous.
              </Typography>
            ) : (
              <List>
                {gs.fileList.map((file) => (
                  <ListItem key={file.id} button onClick={props.selectSpreadSheet(gs, file.id)}>
                    <ListItemIcon>
                      {gs.spreadSheetId === file.id ? <SelectedFileIcon /> : <NotSelectedFileIcon />}
                    </ListItemIcon>
                    <ListItemText primary={file.name} />
                  </ListItem>
                ))}
              </List>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={props.closeSelectSpreadSheetModal} color="secondary">
              Annuler
            </Button>
            <Button onClick={gs.updateFileList} color="primary">
              Actualiser
            </Button>
            <Button onClick={props.openNewSpreadSheetModal} color="primary">
              Nouveau
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </GoogleSyncClient>
  );
}
