import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import { GoogleSyncClient } from './GoogleSync';
import { DatabaseClient } from './Database';
export function ConflictDialog() {
  return (
    <Dialog open onClose={() => {}} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Synchronisation</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Vous venez d'activer la synchronisation ou avez utiliser cette application hors ligne. Si vous avez utiliser
          l'application avec un autre navigateur que celui-ci, choisissez "importer", sinon "exporter".
        </DialogContentText>
      </DialogContent>
      <GoogleSyncClient>
        {(gs) => (
          <DatabaseClient>
            {(db) => (
              <DialogActions>
                <Button
                  onClick={() => {
                    gs.requestImport();
                  }}
                  color="secondary"
                >
                  Importer
                </Button>
                <Button
                  onClick={() => {
                    gs.requestExport(db);
                  }}
                  color="primary"
                >
                  Exporter
                </Button>
              </DialogActions>
            )}
          </DatabaseClient>
        )}
      </GoogleSyncClient>
    </Dialog>
  );
}
