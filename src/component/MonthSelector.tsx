import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import * as React from 'react';
import { uuid } from 'uuidv4';

export default function MonthSelector(props: {
  canBeUndefined?: boolean;
  style?: React.CSSProperties;
  month: number | '';
  label: string;
  onChange(e: React.ChangeEvent<{ name?: string | undefined; value: unknown }>): void;
}) {
  const htmlId = uuid();
  return (
    <FormControl style={{ ...props.style }}>
      <InputLabel id={`${htmlId}-label`}>{props.label}</InputLabel>
      <Select labelId={`${htmlId}-label`} id={htmlId} value={props.month} onChange={props.onChange}>
        {props.canBeUndefined ? <MenuItem value={''}>Indéfinie</MenuItem> : null}
        <MenuItem value={0}>Janvier</MenuItem>
        <MenuItem value={1}>Février</MenuItem>
        <MenuItem value={2}>Mars</MenuItem>
        <MenuItem value={3}>Avril</MenuItem>
        <MenuItem value={4}>Mai</MenuItem>
        <MenuItem value={5}>Juin</MenuItem>
        <MenuItem value={6}>Juillet</MenuItem>
        <MenuItem value={7}>Aout</MenuItem>
        <MenuItem value={8}>Septembre</MenuItem>
        <MenuItem value={9}>Octobre</MenuItem>
        <MenuItem value={10}>Novembre</MenuItem>
        <MenuItem value={11}>Décembre</MenuItem>
      </Select>
    </FormControl>
  );
}
