import { DatabaseState, Expectation } from '../component/Database';
import { LineInGivenMonth } from './LineInGivenMonth';

export function ExpectationRealAmountForGivenMonth(
  exp: Expectation,
  db: DatabaseState,
  filter: {
    year: number;
    month: number;
  },
) {
  return exp.lines
    .map((lineUri) => db.lines[lineUri])
    .filter(LineInGivenMonth(filter))
    .reduce(
      (accumulator, current) =>
        parseFloat((current.amount as unknown) as string) + parseFloat((accumulator as unknown) as string),
      0,
    );
}
