import { Line } from '../component/Database';

export function LineInGivenMonth(filter: {
  year: number;
  month: number;
}): (line: Line, index: number, array: Line[]) => unknown {
  return (line) => {
    const dateFilter = parseInt(`${filter.year}${filter.month}`);
    const lineDate = parseInt(`${line.year}${line.month}`);
    return dateFilter === lineDate;
  };
}
