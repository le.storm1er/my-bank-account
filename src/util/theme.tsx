import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

export const theme = responsiveFontSizes(
  createMuiTheme({
    palette: {
      type: 'dark',
      primary: {
        light: '#ff9d3f',
        main: '#ef6c00',
        dark: '#b53d00',
        contrastText: '#000',
      },
      secondary: {
        light: '#484848',
        main: '#212121',
        dark: '#000000',
        contrastText: '#fff',
      },
    },
    typography: {
      allVariants: {
        color: '#fff',
      },
      body2: {
        color: '#ccc',
      },
    },
  }),
);
