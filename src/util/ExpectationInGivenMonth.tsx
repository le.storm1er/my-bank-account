import { DatabaseState, Expectation } from '../component/Database';

export function ExpectationInGivenMonth(filter: {
  year: number;
  month: number;
}): (value: Expectation, index: number, array: Expectation[]) => unknown {
  return (exp) => {
    const filterAsInt = parseInt(`${filter.year}${filter.month < 10 ? `0${filter.month}` : filter.month}`);
    const expStartAsInt = parseInt(`${exp.startYear}${exp.startMonth}`);
    const expEndAsInt = parseInt(`${exp.endYear || ''}${exp.endMonth || ''}`);

    if (filterAsInt >= expStartAsInt && (!expEndAsInt || filterAsInt <= expEndAsInt)) {
      return true;
    }
    return false;
  };
}

export function ExpectationKeysInGivenMonth(
  filter: {
    year: number;
    month: number;
  },
  db: DatabaseState,
): (value: string, index: number, array: string[]) => unknown {
  return (expUri) => {
    const exp = db.expectations[expUri];
    const filterAsInt = parseInt(`${filter.year}${filter.month < 10 ? `0${filter.month}` : filter.month}`);
    const expStartAsInt = parseInt(`${exp.startYear}${exp.startMonth}`);
    const expEndAsInt = parseInt(`${exp.endYear || ''}${exp.endMonth || ''}`);

    if (filterAsInt >= expStartAsInt && (!expEndAsInt || filterAsInt <= expEndAsInt)) {
      return true;
    }
    return false;
  };
}
