import { MonthState } from '../component/Database';

export default function getPreviousMonthState(monthStates: Record<string, MonthState>, uri: string): MonthState {
  let decomposedUri = uri.split('/');
  let prevYear: number = 0;
  let prevMonth: number = 0;
  if (parseInt(decomposedUri[1]) - 1 < 0) {
    prevYear = parseInt(decomposedUri[0]) - 1;
    prevMonth = 11;
    return (
      monthStates[`${prevYear}/${prevMonth}`] || {
        expectedMovement: 0,
        month: prevMonth,
        realMovement: 0,
        realStartAmount: 0,
        year: prevYear,
      }
    );
  }
  prevYear = parseInt(decomposedUri[0]);
  prevMonth = parseInt(decomposedUri[1]) - 1;
  return (
    monthStates[`${prevYear}/${prevMonth}`] || {
      expectedMovement: 0,
      month: prevMonth,
      realMovement: 0,
      realStartAmount: 0,
      year: prevYear,
    }
  );
}
