import { DatabaseData, Line, Expectation, MonthState } from '../component/Database';
import { LinesRanges, ExpecRanges, MonthRanges } from '../component/GoogleSync';
import { updateCurrentAndNextMonth } from '../util/updateCurrentAndNextMonth';

function toLines(raw?: any[][]): Record<string, Line> {
  if (!raw) {
    return {};
  }
  let ret: Record<string, Line> = {};
  let errors: Array<string> = [];

  for (let row = 1; row < raw.length; row++) {
    let rawLine = raw[row];
    let errorStartCount = errors.length;
    let newLine: Line = {
      year: parseInt(rawLine[1]),
      month: parseInt(rawLine[2]),
      label: rawLine[3] || '',
      amount: parseFloat(rawLine[4]?.replace(/,/g, '.')),
      expectation: rawLine[5] || '',
    };

    if (!rawLine[0]?.length) {
      errors.push(`Lines, row ${row}: ID is invalid, given "${rawLine[0]}"`);
    }
    if (isNaN(newLine.year)) {
      errors.push(`Lines, row ${row}: Year is invalid, given "${newLine.year}"`);
    }
    if (isNaN(newLine.month)) {
      errors.push(`Lines, row ${row}: Month is invalid, given "${newLine.month}"`);
    }
    if (isNaN(newLine.amount)) {
      errors.push(`Lines, row ${row}: Amount is invalid, given "${newLine.amount}"`);
    }

    if (errors.length === errorStartCount) {
      ret = {
        ...ret,
        [rawLine[0]]: newLine,
      };
    }
  }

  if (errors.length > 0) {
    console.warn(errors);
    throw new Error('Unable to import lines.');
  }

  return ret;
}

function toExpectations(raw?: any[][]): Record<string, Expectation> {
  if (!raw) {
    return {};
  }
  let ret: Record<string, Expectation> = {};
  let errors: Array<string> = [];

  for (let row = 1; row < raw.length; row++) {
    let rawExp = raw[row];
    let errorStartCount = errors.length;
    let newExp: Expectation = {
      startYear: parseInt(rawExp[1]),
      startMonth: parseInt(rawExp[2]),
      endYear: parseInt(rawExp[3]) || null,
      endMonth: parseInt(rawExp[4]) || null,
      label: rawExp[5] || '',
      amount: parseFloat(rawExp[6]?.replace(/,/g, '.')),
      lines: [],
    };

    if (!rawExp[0]?.length) {
      errors.push(`Expectations, row ${row}: ID is invalid, given "${rawExp[0]}"`);
    }
    if (isNaN(newExp.startYear)) {
      errors.push(`Expectations, row ${row}: startYear is invalid, given "${newExp.startYear}"`);
    }
    if (isNaN(newExp.startMonth)) {
      errors.push(`Expectations, row ${row}: startMonth is invalid, given "${newExp.startMonth}"`);
    }
    if (newExp.label.length === 0) {
      errors.push(`Expectations, row ${row}: label is invalid, given "${newExp.label}"`);
    }
    if (isNaN(newExp.amount)) {
      errors.push(`Expectations, row ${row}: amount is invalid, given "${newExp.amount}"`);
    }

    if (errors.length === errorStartCount) {
      ret = {
        ...ret,
        [rawExp[0]]: newExp,
      };
    }
  }

  if (errors.length > 0) {
    console.warn(errors);
    throw new Error('Unable to import expectations.');
  }

  return ret;
}

function toMonthStates(raw?: any[][]): Record<string, MonthState> {
  if (!raw) {
    return {};
  }
  let ret: Record<string, MonthState> = {};
  let errors: Array<string> = [];

  for (let row = 1; row < raw.length; row++) {
    let rawMonth = raw[row];
    let errorStartCount = errors.length;
    let newMonth: MonthState = {
      year: parseInt(rawMonth[1]),
      month: parseInt(rawMonth[2]),
      realStartAmount: parseFloat(rawMonth[3]?.replace(/,/g, '.')),
      expectedMovement: parseFloat(rawMonth[4]?.replace(/,/g, '.')),
      realMovement: parseFloat(rawMonth[5]?.replace(/,/g, '.')),
    };

    if (isNaN(newMonth.year) || newMonth.year < 0) {
      errors.push(`MonthState, row ${row}: year is invalid, given "${newMonth.year}"`);
    }
    if (isNaN(newMonth.month) || newMonth.month < 0 || newMonth.month > 11) {
      errors.push(`MonthState, row ${row}: month is invalid, given "${newMonth.month}"`);
    }
    if (isNaN(newMonth.realStartAmount)) {
      errors.push(`MonthState, row ${row}: realStartAmount is invalid, given "${newMonth.realStartAmount}"`);
    }
    if (isNaN(newMonth.expectedMovement)) {
      errors.push(`MonthState, row ${row}: expectedMovement is invalid, given "${newMonth.expectedMovement}"`);
    }
    if (isNaN(newMonth.realMovement)) {
      errors.push(`MonthState, row ${row}: realMovement is invalid, given "${newMonth.realMovement}"`);
    }

    if (errors.length === errorStartCount) {
      ret = {
        ...ret,
        [rawMonth[0]]: newMonth,
      };
    }
  }

  if (errors.length > 0) {
    console.warn(errors);
    throw new Error('Unable to import monthStates.');
  }

  return ret;
}

function registerLineToExpLinks(data: DatabaseData): DatabaseData {
  let errors: Array<string> = [];
  for (const lineUri in data.lines) {
    if (data.lines.hasOwnProperty(lineUri)) {
      const line = data.lines[lineUri];
      if (line.expectation) {
        if (!data.expectations[line.expectation]) {
          errors.push(`Links: Unable to link line ${lineUri} to expectation ${line.expectation}.`);
          continue;
        }
        data.expectations[line.expectation].lines.push(lineUri);
      }
    }
  }
  if (errors.length > 0) {
    console.warn(errors);
    throw new Error('Unable to ensure links in imported data.');
  }
  return data;
}

export default function valueRangesToDatabaseData(data: gapi.client.sheets.ValueRange[]): DatabaseData | false {
  let ret: Partial<DatabaseData> = {};

  try {
    const lineTabName = LinesRanges.substr(0, LinesRanges.indexOf('!')).replace(/'|"/g, '');
    const expeTabName = ExpecRanges.substr(0, ExpecRanges.indexOf('!')).replace(/'|"/g, '');
    const montTabName = MonthRanges.substr(0, MonthRanges.indexOf('!')).replace(/'|"/g, '');
    for (let index = 0; index < data.length; index++) {
      const tab = data[index];
      const tabName = tab.range?.substr(0, tab.range?.indexOf('!')).replace(/'|"/g, '');
      if (tabName === lineTabName) ret.lines = toLines(tab.values);
      if (tabName === expeTabName) ret.expectations = toExpectations(tab.values);
      if (tabName === montTabName) ret.monthStates = toMonthStates(tab.values);
    }
  } catch (error) {
    console.warn(error);
    return false;
  }
  if (!ret.lines || !ret.expectations || !ret.monthStates) {
    console.warn('Incomplete data, missing sheet tab.');
    return false;
  }

  ret = registerLineToExpLinks(ret as DatabaseData);
  const firstYear = Object.values(ret.monthStates || {}).reduce(
    (accu, monthState) => (monthState.year < accu ? monthState.year : accu),
    9999,
  );
  const firstMonth = Object.values(ret.monthStates || {}).reduce(
    (accu, monthState) => (monthState.month < accu ? monthState.month : accu),
    12,
  );
  ret = updateCurrentAndNextMonth(firstYear, firstMonth, ret as DatabaseData);

  return ret as DatabaseData;
}
