import { DatabaseData } from '../component/Database';

/**
 * Will recalculate realMovement and expectedMovement for each month from the given one to the farest possible.
 *
 * @param year The year from where to recalculate numbers from.
 * @param month The year from where to recalculate numbers from.
 * @param state The full DatabaseData.
 */
export function updateCurrentAndNextMonth(year: number, month: number, state: DatabaseData): DatabaseData {
  let givenMonthState = state.monthStates[`${year}/${month}`];
  if (!givenMonthState) {
    return state;
  }
  const currentDate = parseInt(`${givenMonthState.year}${givenMonthState.month}`);
  const givenMonthLines = Object.keys(state.lines)
    .filter((lineUri) => {
      const line = state.lines[lineUri];
      const lineDate = parseInt(`${line.year}${line.month}`);
      return lineDate === currentDate;
    })
    .map((lineUri) => {
      return state.lines[lineUri];
    });
  const givenMonthExpectations = Object.keys(state.expectations)
    .filter((expUri) => {
      const exp = state.expectations[expUri];
      const expStartDate = parseInt(`${exp.startYear}${exp.startMonth}`);
      const expEndDate = parseInt(`${exp.endYear}${exp.endMonth}`);
      return expStartDate <= currentDate && (!expEndDate || expEndDate >= currentDate);
    })
    .map((expUri) => {
      return state.expectations[expUri];
    });
  givenMonthState.realMovement = givenMonthLines.reduce(
    (accu, line) => accu + parseFloat((line.amount as unknown) as string),
    0.0,
  );
  givenMonthState.expectedMovement = givenMonthExpectations.reduce(
    (accu, exp) => accu + parseFloat((exp.amount as unknown) as string),
    0.0,
  );
  state.monthStates[`${year}/${month}`] = givenMonthState;
  if (month < 11) {
    month++;
  } else {
    year++;
    month = 0;
  }
  return updateCurrentAndNextMonth(year, month, state);
}
