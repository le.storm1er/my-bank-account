import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { SnackbarProvider } from 'notistack';
import { theme } from './util/theme';
import { ThemeProvider } from '@material-ui/core';

const rootEl = document.getElementById('root');
ReactDOM.render(
  <ThemeProvider theme={theme}>
    <SnackbarProvider maxSnack={3}>
      <App />
    </SnackbarProvider>
  </ThemeProvider>,
  rootEl,
);

// For hot reload
// See https://github.com/vitaliy-bobrov/angular-hot-loader/issues/5#issuecomment-377785900
if ((module as any).hot) {
  (module as any).hot.accept('./App', () => {
    const NextApp = require('./App').default;
    ReactDOM.render(
      <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={3}>
          <NextApp />
        </SnackbarProvider>
      </ThemeProvider>,
      rootEl,
    );
  });
}
