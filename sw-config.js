// sw-config.js
module.exports = {
  runtimeCaching: [
    {
      urlPattern: '.*',
      handler: 'cacheFirst',
    },
  ],
};
